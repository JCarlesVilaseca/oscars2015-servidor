/****** Object:  Database [Oscars2015]    Script Date: 21/02/2015 22:40:11 ******/
/****** Object:  Table [dbo].[Categories]    Script Date: 21/02/2015 22:40:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Categories](
	[CategoriaId] [int] IDENTITY(1,1) NOT NULL,
	[Nom] [varchar](50) NOT NULL,
	[Guanyador] [int] NULL,
 CONSTRAINT [PK_Categories_CatgeoriaId] PRIMARY KEY CLUSTERED 
(
	[CategoriaId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Nominats]    Script Date: 21/02/2015 22:40:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Nominats](
	[NominatId] [int] IDENTITY(1,1) NOT NULL,
	[Titol] [varchar](80) NOT NULL,
	[Subtitol] [varchar](80) NULL,
	[CategoriaId] [int] NOT NULL,
 CONSTRAINT [PK_Nominats_NominatId] PRIMARY KEY CLUSTERED 
(
	[NominatId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Usuaris]    Script Date: 21/02/2015 22:40:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Usuaris](
	[UsuariId] [int] IDENTITY(1,1) NOT NULL,
	[Nom] [varchar](50) NOT NULL,
	[DataPrimerLogin] [datetime] NULL,
	[DataDarrerLogin] [datetime] NULL,
 CONSTRAINT [PK_Usuaris_UsuariId] PRIMARY KEY CLUSTERED 
(
	[UsuariId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[VotsUsuari]    Script Date: 21/02/2015 22:40:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[VotsUsuari](
	[VotUsuariId] [int] IDENTITY(1,1) NOT NULL,
	[UsuariId] [int] NOT NULL,
	[NominatId] [int] NOT NULL,
	[DataVot] [datetime] NOT NULL,
 CONSTRAINT [PK_VotsUsuari_VotUsuariId] PRIMARY KEY CLUSTERED 
(
	[VotUsuariId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET IDENTITY_INSERT [dbo].[Categories] ON 

GO
INSERT [dbo].[Categories] ([CategoriaId], [Nom], [Guanyador]) VALUES (1, N'Best Picture', NULL)
GO
INSERT [dbo].[Categories] ([CategoriaId], [Nom], [Guanyador]) VALUES (2, N'Actor in a Leading Role', NULL)
GO
INSERT [dbo].[Categories] ([CategoriaId], [Nom], [Guanyador]) VALUES (3, N'Actress in a Leading Role', NULL)
GO
INSERT [dbo].[Categories] ([CategoriaId], [Nom], [Guanyador]) VALUES (4, N'Actor in a Supporting Role', NULL)
GO
INSERT [dbo].[Categories] ([CategoriaId], [Nom], [Guanyador]) VALUES (5, N'Actress in a Supporting Role', NULL)
GO
INSERT [dbo].[Categories] ([CategoriaId], [Nom], [Guanyador]) VALUES (6, N'Animated Feature Film', NULL)
GO
INSERT [dbo].[Categories] ([CategoriaId], [Nom], [Guanyador]) VALUES (7, N'Cinematography', NULL)
GO
INSERT [dbo].[Categories] ([CategoriaId], [Nom], [Guanyador]) VALUES (8, N'Costume Design', NULL)
GO
INSERT [dbo].[Categories] ([CategoriaId], [Nom], [Guanyador]) VALUES (9, N'Directing', NULL)
GO
INSERT [dbo].[Categories] ([CategoriaId], [Nom], [Guanyador]) VALUES (10, N'Documentary Feature', NULL)
GO
INSERT [dbo].[Categories] ([CategoriaId], [Nom], [Guanyador]) VALUES (11, N'Documentary Short Subject', NULL)
GO
INSERT [dbo].[Categories] ([CategoriaId], [Nom], [Guanyador]) VALUES (12, N'Film Editing', NULL)
GO
INSERT [dbo].[Categories] ([CategoriaId], [Nom], [Guanyador]) VALUES (13, N'Foreign Language Film', NULL)
GO
INSERT [dbo].[Categories] ([CategoriaId], [Nom], [Guanyador]) VALUES (14, N'Makeup and HairStyling', NULL)
GO
INSERT [dbo].[Categories] ([CategoriaId], [Nom], [Guanyador]) VALUES (15, N'Music Original Score', NULL)
GO
INSERT [dbo].[Categories] ([CategoriaId], [Nom], [Guanyador]) VALUES (16, N'Music Original Song', NULL)
GO
INSERT [dbo].[Categories] ([CategoriaId], [Nom], [Guanyador]) VALUES (17, N'Production Design', NULL)
GO
INSERT [dbo].[Categories] ([CategoriaId], [Nom], [Guanyador]) VALUES (18, N'Short Film Animated', NULL)
GO
INSERT [dbo].[Categories] ([CategoriaId], [Nom], [Guanyador]) VALUES (19, N'Short Film Live Action', NULL)
GO
INSERT [dbo].[Categories] ([CategoriaId], [Nom], [Guanyador]) VALUES (20, N'Sound Editing', NULL)
GO
INSERT [dbo].[Categories] ([CategoriaId], [Nom], [Guanyador]) VALUES (21, N'Sound Mixing', NULL)
GO
INSERT [dbo].[Categories] ([CategoriaId], [Nom], [Guanyador]) VALUES (22, N'Visual Effects', NULL)
GO
INSERT [dbo].[Categories] ([CategoriaId], [Nom], [Guanyador]) VALUES (23, N'Writing Adapted Screenplay', NULL)
GO
INSERT [dbo].[Categories] ([CategoriaId], [Nom], [Guanyador]) VALUES (24, N'Writing Original Screenplay', NULL)
GO
SET IDENTITY_INSERT [dbo].[Categories] OFF
GO
SET IDENTITY_INSERT [dbo].[Nominats] ON 

GO
INSERT [dbo].[Nominats] ([NominatId], [Titol], [Subtitol], [CategoriaId]) VALUES (1, N'American Sniper', NULL, 1)
GO
INSERT [dbo].[Nominats] ([NominatId], [Titol], [Subtitol], [CategoriaId]) VALUES (3, N'Birdman or (The Unexpected Virtue of Ignorance)', NULL, 1)
GO
INSERT [dbo].[Nominats] ([NominatId], [Titol], [Subtitol], [CategoriaId]) VALUES (5, N'Boyhood', NULL, 1)
GO
INSERT [dbo].[Nominats] ([NominatId], [Titol], [Subtitol], [CategoriaId]) VALUES (6, N'The Grand Budapest Hotel', NULL, 1)
GO
INSERT [dbo].[Nominats] ([NominatId], [Titol], [Subtitol], [CategoriaId]) VALUES (7, N'The Imitation Game', NULL, 1)
GO
INSERT [dbo].[Nominats] ([NominatId], [Titol], [Subtitol], [CategoriaId]) VALUES (8, N'Selma', NULL, 1)
GO
INSERT [dbo].[Nominats] ([NominatId], [Titol], [Subtitol], [CategoriaId]) VALUES (9, N'The Theory of Everything', NULL, 1)
GO
INSERT [dbo].[Nominats] ([NominatId], [Titol], [Subtitol], [CategoriaId]) VALUES (10, N'Whiplash', NULL, 1)
GO
INSERT [dbo].[Nominats] ([NominatId], [Titol], [Subtitol], [CategoriaId]) VALUES (11, N'Steve Carell', N'Foxcatcher', 2)
GO
INSERT [dbo].[Nominats] ([NominatId], [Titol], [Subtitol], [CategoriaId]) VALUES (12, N'Bradley Cooper', N'American Sniper', 2)
GO
INSERT [dbo].[Nominats] ([NominatId], [Titol], [Subtitol], [CategoriaId]) VALUES (13, N'Benedict Cumberbatch', N'The Imitation Game', 2)
GO
INSERT [dbo].[Nominats] ([NominatId], [Titol], [Subtitol], [CategoriaId]) VALUES (14, N'Michael Keaton', N'Birdman or (The Unexpected Virtue of Ignorance)', 2)
GO
INSERT [dbo].[Nominats] ([NominatId], [Titol], [Subtitol], [CategoriaId]) VALUES (15, N'Eddie Redmayne', N'The Theory of Everything', 2)
GO
INSERT [dbo].[Nominats] ([NominatId], [Titol], [Subtitol], [CategoriaId]) VALUES (16, N'Marion Cotillard', N'Two Days, One Night', 3)
GO
INSERT [dbo].[Nominats] ([NominatId], [Titol], [Subtitol], [CategoriaId]) VALUES (17, N'Felicity Jones', N'The Theory of Everything', 3)
GO
INSERT [dbo].[Nominats] ([NominatId], [Titol], [Subtitol], [CategoriaId]) VALUES (18, N'Julianne Moore', N'Still Alice', 3)
GO
INSERT [dbo].[Nominats] ([NominatId], [Titol], [Subtitol], [CategoriaId]) VALUES (20, N'Rosamund Pike', N'Gone Girl', 3)
GO
INSERT [dbo].[Nominats] ([NominatId], [Titol], [Subtitol], [CategoriaId]) VALUES (21, N'Reese Witherspoon', N'Wild', 3)
GO
INSERT [dbo].[Nominats] ([NominatId], [Titol], [Subtitol], [CategoriaId]) VALUES (22, N'Robert Duvall', N'The Judge', 4)
GO
INSERT [dbo].[Nominats] ([NominatId], [Titol], [Subtitol], [CategoriaId]) VALUES (23, N'Ethan Hawke', N'Boyhood', 4)
GO
INSERT [dbo].[Nominats] ([NominatId], [Titol], [Subtitol], [CategoriaId]) VALUES (24, N'Edward Norton', N'Birdman or (The Unexpected Virtue of Ignorance)', 4)
GO
INSERT [dbo].[Nominats] ([NominatId], [Titol], [Subtitol], [CategoriaId]) VALUES (25, N'Mark Ruffalo', N'Foxcatcher', 4)
GO
INSERT [dbo].[Nominats] ([NominatId], [Titol], [Subtitol], [CategoriaId]) VALUES (26, N'J.K. Simmons', N'Whiplash', 4)
GO
INSERT [dbo].[Nominats] ([NominatId], [Titol], [Subtitol], [CategoriaId]) VALUES (27, N'Patricia Arquette', N'Boyhood', 5)
GO
INSERT [dbo].[Nominats] ([NominatId], [Titol], [Subtitol], [CategoriaId]) VALUES (28, N'Laura Dern', N'Wild', 5)
GO
INSERT [dbo].[Nominats] ([NominatId], [Titol], [Subtitol], [CategoriaId]) VALUES (29, N'Keira Knightley', N'The Imitation Game', 5)
GO
INSERT [dbo].[Nominats] ([NominatId], [Titol], [Subtitol], [CategoriaId]) VALUES (30, N'Emma Stone', N'Birdman or (The Unexpected Virtue of Ignorance)', 5)
GO
INSERT [dbo].[Nominats] ([NominatId], [Titol], [Subtitol], [CategoriaId]) VALUES (31, N'Meryl Streep', N'Into the Woods', 5)
GO
INSERT [dbo].[Nominats] ([NominatId], [Titol], [Subtitol], [CategoriaId]) VALUES (32, N'Big Hero 6', N'Don Hall, Chris Williams and Roy Conli', 6)
GO
INSERT [dbo].[Nominats] ([NominatId], [Titol], [Subtitol], [CategoriaId]) VALUES (33, N'The Boxtrolls', N'Anthony Stacchi, Graham Annable and Travis Knight', 6)
GO
INSERT [dbo].[Nominats] ([NominatId], [Titol], [Subtitol], [CategoriaId]) VALUES (34, N'How to Train Your Dragon 2', N'Dean DeBlois and Bonnie Arnold', 6)
GO
INSERT [dbo].[Nominats] ([NominatId], [Titol], [Subtitol], [CategoriaId]) VALUES (35, N'Song of the Sea', N'Tomm Moore and Paul Young', 6)
GO
INSERT [dbo].[Nominats] ([NominatId], [Titol], [Subtitol], [CategoriaId]) VALUES (36, N'The Tale of the Princess Kaguya', N'Isao Takahata and Yoshiaki Nishimura
', 6)
GO
INSERT [dbo].[Nominats] ([NominatId], [Titol], [Subtitol], [CategoriaId]) VALUES (37, N'Birdman or (The Unexpected Virtue of Ignorance)', N'Emmanuel Lubezki', 7)
GO
INSERT [dbo].[Nominats] ([NominatId], [Titol], [Subtitol], [CategoriaId]) VALUES (38, N'The Grand Budapest Hotel', N'Robert Yeoman', 7)
GO
INSERT [dbo].[Nominats] ([NominatId], [Titol], [Subtitol], [CategoriaId]) VALUES (39, N'Ida', N'Lukasz Zal and Ryszard Lenczewski', 7)
GO
INSERT [dbo].[Nominats] ([NominatId], [Titol], [Subtitol], [CategoriaId]) VALUES (40, N'Mr. Turner', N'Dick Pope', 7)
GO
INSERT [dbo].[Nominats] ([NominatId], [Titol], [Subtitol], [CategoriaId]) VALUES (41, N'Unbroken', N'Roger Deakins', 7)
GO
INSERT [dbo].[Nominats] ([NominatId], [Titol], [Subtitol], [CategoriaId]) VALUES (42, N'The Grand Budapest Hotel', N'Milena Canonero', 8)
GO
INSERT [dbo].[Nominats] ([NominatId], [Titol], [Subtitol], [CategoriaId]) VALUES (43, N'Inherent Vice', N'Mark Bridges', 8)
GO
INSERT [dbo].[Nominats] ([NominatId], [Titol], [Subtitol], [CategoriaId]) VALUES (44, N'Into the Woods', N'Colleen Atwood', 8)
GO
INSERT [dbo].[Nominats] ([NominatId], [Titol], [Subtitol], [CategoriaId]) VALUES (45, N'Maleficent', N'Anna B. Sheppard', 8)
GO
INSERT [dbo].[Nominats] ([NominatId], [Titol], [Subtitol], [CategoriaId]) VALUES (46, N'Mr. Turner', N'Jacqueline Durran', 8)
GO
INSERT [dbo].[Nominats] ([NominatId], [Titol], [Subtitol], [CategoriaId]) VALUES (47, N'Birdman or (The Unexpected Virtue of Ignorance)', N'Alejandro G. Iñárritu', 9)
GO
INSERT [dbo].[Nominats] ([NominatId], [Titol], [Subtitol], [CategoriaId]) VALUES (48, N'Boyhood', N'Richard Linklater', 9)
GO
INSERT [dbo].[Nominats] ([NominatId], [Titol], [Subtitol], [CategoriaId]) VALUES (50, N'Foxcatcher', N'Bennett Miller', 9)
GO
INSERT [dbo].[Nominats] ([NominatId], [Titol], [Subtitol], [CategoriaId]) VALUES (51, N'The Grand Budapest Hotel', N'Wes Anderson', 9)
GO
INSERT [dbo].[Nominats] ([NominatId], [Titol], [Subtitol], [CategoriaId]) VALUES (52, N'The Imitation Game', N'Morten Tyldum', 9)
GO
INSERT [dbo].[Nominats] ([NominatId], [Titol], [Subtitol], [CategoriaId]) VALUES (53, N'CitizenFour', N'Laura Poitras, Mathilde Bonnefoy and Dirk Wilutzky', 10)
GO
INSERT [dbo].[Nominats] ([NominatId], [Titol], [Subtitol], [CategoriaId]) VALUES (54, N'Finding Vivian Maier', N'John Maloof and Charlie Siskel', 10)
GO
INSERT [dbo].[Nominats] ([NominatId], [Titol], [Subtitol], [CategoriaId]) VALUES (55, N'Last Days in Vietnam', N'Rory Kennedy and Keven McAlester', 10)
GO
INSERT [dbo].[Nominats] ([NominatId], [Titol], [Subtitol], [CategoriaId]) VALUES (58, N'The Salt of the Earth', N'Wim Wenders, Juliano Ribeiro Salgado...', 10)
GO
INSERT [dbo].[Nominats] ([NominatId], [Titol], [Subtitol], [CategoriaId]) VALUES (59, N'Virunga', N'Orlando von Einsiedel and Joanna Natasegara', 10)
GO
INSERT [dbo].[Nominats] ([NominatId], [Titol], [Subtitol], [CategoriaId]) VALUES (60, N'Crisis Hotline: Veterans Press 1', N'Ellen Goosenberg Kent and Dana Perry', 11)
GO
INSERT [dbo].[Nominats] ([NominatId], [Titol], [Subtitol], [CategoriaId]) VALUES (61, N'Joanna', N'Aneta Kopacz', 11)
GO
INSERT [dbo].[Nominats] ([NominatId], [Titol], [Subtitol], [CategoriaId]) VALUES (62, N'Our Curse', N'Tomasz Sliwinski and Maciej Slesicki', 11)
GO
INSERT [dbo].[Nominats] ([NominatId], [Titol], [Subtitol], [CategoriaId]) VALUES (63, N'The Reaper (La Parka)', N'Gabriel Serra Arguello', 11)
GO
INSERT [dbo].[Nominats] ([NominatId], [Titol], [Subtitol], [CategoriaId]) VALUES (64, N'White Earth', N'J. Christian Jensen', 11)
GO
INSERT [dbo].[Nominats] ([NominatId], [Titol], [Subtitol], [CategoriaId]) VALUES (65, N'American Sniper', N'Joel Cox and Gary D. Roach', 12)
GO
INSERT [dbo].[Nominats] ([NominatId], [Titol], [Subtitol], [CategoriaId]) VALUES (66, N'Boyhood', N'Sandra Adair', 12)
GO
INSERT [dbo].[Nominats] ([NominatId], [Titol], [Subtitol], [CategoriaId]) VALUES (67, N'The Grand Budapest Hotel', N'Barney Pilling', 12)
GO
INSERT [dbo].[Nominats] ([NominatId], [Titol], [Subtitol], [CategoriaId]) VALUES (68, N'The Imitation Game', N'William Goldenberg', 12)
GO
INSERT [dbo].[Nominats] ([NominatId], [Titol], [Subtitol], [CategoriaId]) VALUES (69, N'Whiplash', N'Tom Cross', 12)
GO
INSERT [dbo].[Nominats] ([NominatId], [Titol], [Subtitol], [CategoriaId]) VALUES (70, N'Ida', N'Poland; Directed by Pawel Pawlikowski', 13)
GO
INSERT [dbo].[Nominats] ([NominatId], [Titol], [Subtitol], [CategoriaId]) VALUES (71, N'Leviathan', N'Russia; Directed by Andrey Zvyagintsev', 13)
GO
INSERT [dbo].[Nominats] ([NominatId], [Titol], [Subtitol], [CategoriaId]) VALUES (72, N'Tangerines', N'Estonia; Directed by Zaza Urushadze', 13)
GO
INSERT [dbo].[Nominats] ([NominatId], [Titol], [Subtitol], [CategoriaId]) VALUES (73, N'Timbuktu', N'Mauritania; Directed by Abderrahmane Sissako', 13)
GO
INSERT [dbo].[Nominats] ([NominatId], [Titol], [Subtitol], [CategoriaId]) VALUES (74, N'Wild Tales', N'Argentina; Directed by Damián Szifron', 13)
GO
INSERT [dbo].[Nominats] ([NominatId], [Titol], [Subtitol], [CategoriaId]) VALUES (75, N'Foxcatcher', N'Bill Corso and Dennis Liddiard', 14)
GO
INSERT [dbo].[Nominats] ([NominatId], [Titol], [Subtitol], [CategoriaId]) VALUES (76, N'The Grand Budapest Hotel', N'Frances Hannon and Mark Coulier', 14)
GO
INSERT [dbo].[Nominats] ([NominatId], [Titol], [Subtitol], [CategoriaId]) VALUES (77, N'Guardians of the Galaxy', N'Elizabeth Yianni-Georgiou and David White', 14)
GO
INSERT [dbo].[Nominats] ([NominatId], [Titol], [Subtitol], [CategoriaId]) VALUES (78, N'The Grand Budapest Hotel', N'Alexandre Desplat', 15)
GO
INSERT [dbo].[Nominats] ([NominatId], [Titol], [Subtitol], [CategoriaId]) VALUES (79, N'The Imitation Game', N'Alexandre Desplat', 15)
GO
INSERT [dbo].[Nominats] ([NominatId], [Titol], [Subtitol], [CategoriaId]) VALUES (80, N'Interstellar', N'Hans Zimmer', 15)
GO
INSERT [dbo].[Nominats] ([NominatId], [Titol], [Subtitol], [CategoriaId]) VALUES (81, N'Mr. Turner', N'Gary Yershon', 15)
GO
INSERT [dbo].[Nominats] ([NominatId], [Titol], [Subtitol], [CategoriaId]) VALUES (82, N'The Theory of Everything', N'Jóhann Jóhannsson', 15)
GO
INSERT [dbo].[Nominats] ([NominatId], [Titol], [Subtitol], [CategoriaId]) VALUES (83, N'"Everything Is Awesome" from THE LEGO MOVIE', N'Music and Lyric by Shawn Patterson', 16)
GO
INSERT [dbo].[Nominats] ([NominatId], [Titol], [Subtitol], [CategoriaId]) VALUES (84, N'"Glory" from SELMA', N'Music and Lyric by John Stephens and Lonnie Lynn', 16)
GO
INSERT [dbo].[Nominats] ([NominatId], [Titol], [Subtitol], [CategoriaId]) VALUES (85, N'"Grateful" from BEYOND THE LIGHTS', N'Music and Lyric by Diane Warren', 16)
GO
INSERT [dbo].[Nominats] ([NominatId], [Titol], [Subtitol], [CategoriaId]) VALUES (87, N'"I''m Not Gonna Miss You" from GLEN CAMPBELL...I''LL BE ME', N'Music and Lyric by Glen Campbell and Julian Raymond', 16)
GO
INSERT [dbo].[Nominats] ([NominatId], [Titol], [Subtitol], [CategoriaId]) VALUES (88, N'"Lost Stars" from BEGIN AGAIN', N'Music and Lyric by Gregg Alexander and Danielle Brisebois', 16)
GO
INSERT [dbo].[Nominats] ([NominatId], [Titol], [Subtitol], [CategoriaId]) VALUES (89, N'The Grand Budapest Hotel', N'Adam Stockhausen (Production Design); Anna Pinnock (Set Decoration)', 17)
GO
INSERT [dbo].[Nominats] ([NominatId], [Titol], [Subtitol], [CategoriaId]) VALUES (90, N'The Imitation Game', N'Maria Djurkovic (Production Design); Tatiana Macdonald (Set Decoration)
', 17)
GO
INSERT [dbo].[Nominats] ([NominatId], [Titol], [Subtitol], [CategoriaId]) VALUES (91, N'Interstellar', N'Nathan Crowley (Production Design); Gary Fettis (Set Decoration)', 17)
GO
INSERT [dbo].[Nominats] ([NominatId], [Titol], [Subtitol], [CategoriaId]) VALUES (92, N'Into the Woods', N'Dennis Gassner (Production Design); Anna Pinnock (Set Decoration)', 17)
GO
INSERT [dbo].[Nominats] ([NominatId], [Titol], [Subtitol], [CategoriaId]) VALUES (93, N'Mr. Turner', N'Suzie Davies (Production Design); Charlotte Watts (Set Decoration)', 17)
GO
INSERT [dbo].[Nominats] ([NominatId], [Titol], [Subtitol], [CategoriaId]) VALUES (94, N'The Bigger Picture', N'Daisy Jacobs and Christopher Hees', 18)
GO
INSERT [dbo].[Nominats] ([NominatId], [Titol], [Subtitol], [CategoriaId]) VALUES (95, N'The Dam Keeper', N'Robert Kondo and Dice Tsutsumi', 18)
GO
INSERT [dbo].[Nominats] ([NominatId], [Titol], [Subtitol], [CategoriaId]) VALUES (96, N'Feast', N'Patrick Osborne and Kristina Reed', 18)
GO
INSERT [dbo].[Nominats] ([NominatId], [Titol], [Subtitol], [CategoriaId]) VALUES (97, N'Me and My Moulton', N'Torill Kove', 18)
GO
INSERT [dbo].[Nominats] ([NominatId], [Titol], [Subtitol], [CategoriaId]) VALUES (98, N'A Single Life', N'Joris Oprins', 18)
GO
INSERT [dbo].[Nominats] ([NominatId], [Titol], [Subtitol], [CategoriaId]) VALUES (99, N'Aya', N'Oded Binnun and Mihal Brezis', 19)
GO
INSERT [dbo].[Nominats] ([NominatId], [Titol], [Subtitol], [CategoriaId]) VALUES (100, N'Boogaloo and Graham', N'Michael Lennox and Ronan Blaney', 19)
GO
INSERT [dbo].[Nominats] ([NominatId], [Titol], [Subtitol], [CategoriaId]) VALUES (101, N'Butter Lamp (La Lampe au Beurre de Yak)', N'Hu Wei and Julien Féret', 19)
GO
INSERT [dbo].[Nominats] ([NominatId], [Titol], [Subtitol], [CategoriaId]) VALUES (102, N'Parvaneh', N'Talkhon Hamzavi and Stefan Eichenberger', 19)
GO
INSERT [dbo].[Nominats] ([NominatId], [Titol], [Subtitol], [CategoriaId]) VALUES (103, N'The Phone Call', N'Mat Kirkby and James Lucas', 19)
GO
INSERT [dbo].[Nominats] ([NominatId], [Titol], [Subtitol], [CategoriaId]) VALUES (105, N'American Sniper', N'Alan Robert Murray and Bub Asman', 20)
GO
INSERT [dbo].[Nominats] ([NominatId], [Titol], [Subtitol], [CategoriaId]) VALUES (106, N'Birdman or (The Unexpected Virtue of Ignorance)', N'Martin Hernández and Aaron Glascock', 20)
GO
INSERT [dbo].[Nominats] ([NominatId], [Titol], [Subtitol], [CategoriaId]) VALUES (107, N'The Hobbit: The Battle of the Five Armies', N'Brent Burge and Jason Canovas', 20)
GO
INSERT [dbo].[Nominats] ([NominatId], [Titol], [Subtitol], [CategoriaId]) VALUES (108, N'Interstellar', N'Richard King', 20)
GO
INSERT [dbo].[Nominats] ([NominatId], [Titol], [Subtitol], [CategoriaId]) VALUES (109, N'Unbroken', N'Becky Sullivan and Andrew DeCristofaro', 20)
GO
INSERT [dbo].[Nominats] ([NominatId], [Titol], [Subtitol], [CategoriaId]) VALUES (110, N'American Sniper', N'John Reitz, Gregg Rudloff and Walt Martin', 21)
GO
INSERT [dbo].[Nominats] ([NominatId], [Titol], [Subtitol], [CategoriaId]) VALUES (111, N'Birdman or (The Unexpected Virtue of Ignorance)', N'Jon Taylor, Frank A. Montaño and Thomas Varga', 21)
GO
INSERT [dbo].[Nominats] ([NominatId], [Titol], [Subtitol], [CategoriaId]) VALUES (112, N'Interstellar', N'Gary A. Rizzo, Gregg Landaker and Mark Weingarten', 21)
GO
INSERT [dbo].[Nominats] ([NominatId], [Titol], [Subtitol], [CategoriaId]) VALUES (113, N'Unbroken', N'Jon Taylor, Frank A. Montaño and David Lee', 21)
GO
INSERT [dbo].[Nominats] ([NominatId], [Titol], [Subtitol], [CategoriaId]) VALUES (114, N'Whiplash', N'Craig Mann, Ben Wilkins and Thomas Curley', 21)
GO
INSERT [dbo].[Nominats] ([NominatId], [Titol], [Subtitol], [CategoriaId]) VALUES (115, N'Captain America: The Winter Soldier', N'Dan DeLeeuw, Russell Earl, Bryan Grill and Dan Sudick', 22)
GO
INSERT [dbo].[Nominats] ([NominatId], [Titol], [Subtitol], [CategoriaId]) VALUES (116, N'Dawn of the Planet of the Apes', N'Joe Letteri, Dan Lemmon, Daniel Barrett and Erik Winquist', 22)
GO
INSERT [dbo].[Nominats] ([NominatId], [Titol], [Subtitol], [CategoriaId]) VALUES (117, N'Guardians of the Galaxy', N'Stephane Ceretti, Nicolas Aithadi, Jonathan Fawkner and Paul Corbould', 22)
GO
INSERT [dbo].[Nominats] ([NominatId], [Titol], [Subtitol], [CategoriaId]) VALUES (118, N'Interstellar', N'Paul Franklin, Andrew Lockley, Ian Hunter and Scott Fisher', 22)
GO
INSERT [dbo].[Nominats] ([NominatId], [Titol], [Subtitol], [CategoriaId]) VALUES (119, N'X-Men: Days of Future Past', N'Richard Stammers, Lou Pecora, Tim Crosbie and Cameron Waldbauer', 22)
GO
INSERT [dbo].[Nominats] ([NominatId], [Titol], [Subtitol], [CategoriaId]) VALUES (120, N'American Sniper', N'Written by Jason Hall', 23)
GO
INSERT [dbo].[Nominats] ([NominatId], [Titol], [Subtitol], [CategoriaId]) VALUES (121, N'The Imitation Game', N'Written by Graham Moore', 23)
GO
INSERT [dbo].[Nominats] ([NominatId], [Titol], [Subtitol], [CategoriaId]) VALUES (122, N'Inherent Vice', N'Written for the screen by Paul Thomas Anderson', 23)
GO
INSERT [dbo].[Nominats] ([NominatId], [Titol], [Subtitol], [CategoriaId]) VALUES (123, N'The Theory of Everything', N'Screenplay by Anthony McCarten', 23)
GO
INSERT [dbo].[Nominats] ([NominatId], [Titol], [Subtitol], [CategoriaId]) VALUES (124, N'Whiplash', N'Written by Damien Chazelle', 23)
GO
INSERT [dbo].[Nominats] ([NominatId], [Titol], [Subtitol], [CategoriaId]) VALUES (127, N'Birdman or (The Unexpected Virtue of Ignorance)', N'Written by Alejandro G. Iñárritu, Nicolás Giacobone, Alexander Dinelaris ...', 24)
GO
INSERT [dbo].[Nominats] ([NominatId], [Titol], [Subtitol], [CategoriaId]) VALUES (128, N'Boyhood', N'Written by Richard Linklater', 24)
GO
INSERT [dbo].[Nominats] ([NominatId], [Titol], [Subtitol], [CategoriaId]) VALUES (129, N'Foxcatcher', N'Written by E. Max Frye and Dan Futterman', 24)
GO
INSERT [dbo].[Nominats] ([NominatId], [Titol], [Subtitol], [CategoriaId]) VALUES (130, N'The Grand Budapest Hotel', N'Screenplay by Wes Anderson; Story by Wes Anderson & Hugo Guinness', 24)
GO
INSERT [dbo].[Nominats] ([NominatId], [Titol], [Subtitol], [CategoriaId]) VALUES (131, N'Nightcrawler', N'Written by Dan Gilroy', 24)
GO
SET IDENTITY_INSERT [dbo].[Nominats] OFF
GO
ALTER TABLE [dbo].[Nominats]  WITH CHECK ADD  CONSTRAINT [FK_Nominats_Categories] FOREIGN KEY([CategoriaId])
REFERENCES [dbo].[Categories] ([CategoriaId])
GO
ALTER TABLE [dbo].[Categories] WITH CHECK ADD CONSTRAINT [FK_Categories_Nominats] FOREIGN KEY ([Guanyador]) 
REFERENCES [dbo].[Nominats] ([NominatId])
GO
ALTER TABLE [dbo].[Nominats] CHECK CONSTRAINT [FK_Nominats_Categories]
GO
ALTER TABLE [dbo].[VotsUsuari]  WITH CHECK ADD  CONSTRAINT [FK_VotsUsuari_Nominats] FOREIGN KEY([NominatId])
REFERENCES [dbo].[Nominats] ([NominatId])
GO
ALTER TABLE [dbo].[VotsUsuari] CHECK CONSTRAINT [FK_VotsUsuari_Nominats]
GO
ALTER TABLE [dbo].[VotsUsuari]  WITH CHECK ADD  CONSTRAINT [FK_VotsUsuari_Usuaris] FOREIGN KEY([UsuariId])
REFERENCES [dbo].[Usuaris] ([UsuariId])
GO
ALTER TABLE [dbo].[VotsUsuari] CHECK CONSTRAINT [FK_VotsUsuari_Usuaris]
GO
ALTER DATABASE [Oscars2015] SET  READ_WRITE 
GO
